#  TP1 Exercice 6

## Objectif 
L'objectif de cet exercice est de faire des tests automatisés des différents fonctions
des packages utilisés.

## Realisation 
J'ai créé deux packages, un permettant de stocker le script de SimpleComplexCalculator et un autre package contenant un script de test.

## Organisation
voici l'arborescence de notre projet :
```
.
├── Package_Calculator
│   ├── Calculator.py
│   ├── __init__.py
│   └── __pycache__
│       
├── Package_Test
│   ├── Calculator_test.py
│   └── __init__.py
│ 
└── README.md
```


## Lancement
il faut d'abord entrer la commande :  
  
    export PYTHONPATH=$PYTHONPATH:'.'

puis on execute la commande :  

    python3 ./Package_Test/exo6.py

## Résultat
```theotime.perrichet@tpi10:~/tp1_ex6$ python3 ./Package_Test/exo6.py
.............
----------------------------------------------------------------------
Ran 13 tests in 0.001s
```


