#!/usr/bin/env python
# coding: utf-8
"""
author : theotime Perrichet

-Ce code permet de faire des tests automatisés des différents fonctions
des packages utilisés
"""
import unittest

from Package_Calculator.Calculator import SimpleComplexCalculator


class Test_Sum(unittest.TestCase):
    """Cette classe permet de vérifier le bon fonctionnement de la méthode
    sum_complex de la classe SimpleCalculator"""
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleComplexCalculator()

    def test_Sum2Tup(self):
        """somme de deux tuples, le résultat doit être [2,7]"""
        resultat = self.calculator.sum_complex([1,5], [1,2])
        self.assertEqual(resultat, [2,7])
        

    def test_SumStr(self):
        """Somme de deux tuples contenant un entier et un caractère, le résultat 
        doit être ERROR"""
        resultat = self.calculator.sum_complex([2,7], ["2",7])
        self.assertEqual(resultat, "ERROR")
    
    def test_SumNeg(self):
        """Somme entre un entier positif et un entier négatif
        le résultat doit être [0,-12]"""
        resultat = self.calculator.sum_complex([-1,-10], [1,-2])
        self.assertEqual(resultat, [0,-12])


class Test_Sub(unittest.TestCase):
    """Cette classe permet de vérifier le bon focntionnement de la méthode
    susbtract de la classe SimpleCalculator"""
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleComplexCalculator()

    def test_Sub2Tup(self):
        """Soustraction de deux tuples, le résultat doit être [0,3]"""
        resultat = self.calculator.substract_complex([1,5], [1,2])
        self.assertEqual(resultat, [0,3])

    def test_SubStr(self):
        """Soustraction de deux tuples contenant un entier et un caractère, le résultat 
        doit être ERROR"""
        resultat = self.calculator.substract_complex(["1",5], [1,"2"])
        self.assertEqual(resultat, "ERROR")

    def test_SumNeg(self):
        """Soustraction entre un entier positif et un entier négatif
        le résultat doit être [2,-7]"""
        resultat = self.calculator.substract_complex([1,-5], [-1,2])
        self.assertEqual(resultat, [2,-7])


class Testmultiply_complex(unittest.TestCase):

    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleComplexCalculator()

    def test_Mult2Tup(self):
        """Multiplication entre deux entiers, le résultat doit être [-9,7]"""
        resultat = self.calculator.multiply_complex([1,5], [1,2])
        self.assertEqual(resultat, [-9,7])

    def test_MultStr(self):
        """"Multiplication entre un entier et un caractère, le résultat
        doit être ERROR"""
        resultat = self.calculator.multiply_complex([1,5], ["1",2])
        self.assertEqual(resultat, "ERROR")

    def test_MultNeg(self):
        """Multiplication entre deux entiers négatifs, le résultat doit 
        être [9,7]"""
        resultat = self.calculator.multiply_complex([1,-5], [-1,2])
        self.assertEqual(resultat, [9,7])


class Testdivide_complex(unittest.TestCase):

    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleComplexCalculator()

    def test_Div2Tup(self):
        """Division entre deux entier, on doit obtenir 2"""
        resultat = self.calculator.divide_complex([1,5], [1,2])
        self.assertEqual(resultat, [2.2,0.6])

    def test_Div_Zero(self):
        """Division par 0, nous devons obtenir un message d'erreur"""
        resultat = self.calculator.divide_complex([1,5], [0,0])
        self.assertEqual(resultat, "vous avez essayé de diviser par zéro")

    def test_DivNeg(self):
        """DIvision d'un entier négatif par un positif, on doit obtenir [-2.2,0.6]"""
        resultat = self.calculator.divide_complex([1,-5], [-1,2])
        self.assertEqual(resultat, [-2.2,0.6])

    def test_DivStr(self):
        """Division d'un entier par un caractère, on doit obtenir ERROR"""
        resultat = self.calculator.divide_complex([1,5], [1,"2"])
        self.assertEqual(resultat, "ERROR")



if __name__ == '__main__':
    unittest.main()



